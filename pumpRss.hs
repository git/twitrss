{- Horrible screen scraper to generate a RSS feed for a pump.io user.
 - Provide the url to the user to scrape. RSS feed is output to stdout.
 -
 - This was written as a stop-gap measure until issue #55 gets dealt with.
 - Also as an opportunity to get familiar with generating RSS feeds and
 - parsing TagSoup from Haskell.
 -
 - This will break. When it does, send me a patch. Any other mail about
 - this will be ignored, unless it's hate mail from Evan. <id@joeyh.name>
 -
 - This pounds on the pump server. Please don't run it frequently.
 -
 - Known bugs: Does not include a post date in the feed. 
 - I can't be arsed to convert between incompatable date formats
 - yet again.
 -
 -
 -
 - Don't use this. Pump has proper rss support now!
 -
 -
 - License: GPL-3+
 -}

import Text.HTML.TagSoup
import Network.HTTP.Conduit
import Text.RSS.Syntax
import Text.RSS.Export
import Text.XML.Light.Output
import System.Environment
import Control.Monad
import Data.String.Utils
import Data.ByteString.Lazy.UTF8 (toString, ByteString)

-- All the tag soup that makes up a note.
type Note = [Tag String]

type HtmlString = String

main = mapM_ dumpFeed =<< getArgs

dumpFeed :: URLString -> IO ()
dumpFeed url = do
	page <- downloadUrl url
	putStr $ showElement $ xmlRSS $ genFeed url $ toString page

downloadUrl :: URLString -> IO ByteString
downloadUrl url = withManager $ \man -> do
	url' <- parseUrl url
	let req = url' { responseTimeout = Nothing }
	fmap responseBody $ httpLbs req man

genFeed :: URLString -> String -> RSS
genFeed url page = (nullRSS title url)
	{ rssChannel=(nullChannel title url) { rssItems = items } }
  where
	title = last $ filter (not . null) $ split "/" url
	items = genItems page

noteItem :: Note -> RSSItem
noteItem note = (nullItem "")
	{ rssItemLink = Just $ extractNoteUrl note
	, rssItemDescription = Just $ extractNoteBody note
	, rssItemTitle = Just $ extractNoteTitle note
	}

genItems :: String -> [RSSItem]
genItems = map noteItem . extractNotes . canonicalizeTags . parseTags

{- Each main note in the page is currently a <li> -}
extractNotes :: [Tag String] -> [Note]
extractNotes tags = map (takeWhile (~/= "</li>")) $
	sections (~== "<li class=\"media activity major headless\">") tags

{- The body of a note is currently inside a particular <div>
 -
 - This assumes that the body does not contain nested divs. -}
extractNoteBody :: Note -> HtmlString
extractNoteBody = renderTags .
	takeWhile (~/= "</div>") .
	drop 1 .
	dropWhile (~/= "<div class=activity-content>")

{- RSS requires a title, but notes don't have titles.
 - Workaround: Use the post date as the title. -}
extractNoteTitle :: Note -> String
extractNoteTitle note = fromAttrib "title" $
	sections (~== "<abbr class=easydate>") note !! 0 !! 0

{- The url of a note is currently in the first <a> inside it. -}
extractNoteUrl :: Note -> URLString
extractNoteUrl note = fromAttrib "href" $
	sections (~== "<a>") note !! 0 !! 0
